/**
 * Delay for a number of milliseconds
 */
function sleep(delay) {
    console.log("Bussy wating for", delay, "ms");
    var start = new Date().getTime();
    while (new Date().getTime() < start + delay) ;
}

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

function getSupportedCurrencies() {
    return ["USD", "EUR", "GBP", "JPY"]
}

function getExchangeRate(from, to) {
    console.log("Requesting exchange rate from", from, "to", to)
    sleep(Math.floor(2300 * Math.random()));
    let fuzzy = getRandomArbitrary(0.9, 1.1);
    let rate = 0;
    if (from === to) {
        return 1;
    } else if (from === "USD" && to === "EUR") {
        return fuzzy * 0.9117;
    } else if (from === "USD" && to === "GBP") {
        return fuzzy * 0.8292;
    } else if (from === "USD" && to === "JPY") {
        return fuzzy * 106.163;
    } else if (from === "EUR" && to === "USD") {
        return 1 / getExchangeRate(to, from);
    } else if (from === "EUR" && to === "GBP") {
        return fuzzy * 0.9094;
    } else if (from === "EUR" && to === "JPY") {
        return fuzzy * 116.441;
    } else if (from === "GBP" && to === "USD") {
        return 1 / getExchangeRate(to, from);
    } else if (from === "GBP" && to === "EUR") {
        return 1 / getExchangeRate(to, from);
    } else if (from === "GBP" && to === "JPY") {
        return fuzzy * 128.062;
    } else if (from === "JPY" && to === "USD") {
        return 1 / getExchangeRate(to, from);
    } else if (from === "JPY" && to === "EUR") {
        return 1 / getExchangeRate(to, from);
    } else if (from === "JPY" && to === "GBP") {
        return 1 / getExchangeRate(to, from);
    }
    console.log("Wrong combination");
}
